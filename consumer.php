<?php

class Consumer {

    public function sendPost() {
        $data = array("tittle" => "un libro", "isbn" => "998-84-8181-8", "author" => "un autor :)");
        $ch = curl_init("http://localhost/slimRest/index.php/books");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($ch);
        curl_close($ch);
        if (!$response) {
            return false;
        } else {
            var_dump($response);
        }
    }

    public function sendPut($id) {
        $data = array("tittle" => "libro actualizado", "isbn" => "978-74-8181-2", "author" => "juan", "id" => $id);
        $ch = curl_init("http://localhost/slimRest/index.php/books");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($ch);
        curl_close($ch);
        if (!$response) {
            return false;
        } else {
            var_dump($response);
        }
    }

    public function sendGetAll() {
        $ch = curl_init("http://localhost/slimRest/index.php/books");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $response = curl_exec($ch);
        curl_close($ch);
        if (!$response) {
            return false;
        } else {
            var_dump($response);
        }
    }

    public function sendGetById($id) {
        $ch = curl_init("http://localhost/slimRest/index.php/books/$id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $response = curl_exec($ch);
        curl_close($ch);
        if (!$response) {
            return false;
        } else {
            var_dump($response);
        }
    }

    public function sendDelete() {
        $ch = curl_init("http://localhost/slimRest/index.php/books/$id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $response = curl_exec($ch);
        curl_close($ch);
        if (!$response) {
            return false;
        } else {
            var_dump($response);
        }
    }

}

$curl = new Consumer();
$curl->sendGetAll();

//echo "hola";
